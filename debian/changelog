octave-linear-algebra (2.2.3-4) unstable; urgency=medium

  * Team upload

  * d/copyright: Accentuate my family name
  * d/copyright: Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 13:27:14 -0300

octave-linear-algebra (2.2.3-3) unstable; urgency=medium

  * Team upload

  * d/octave-linear-algebra.lintian-overrides: Drop obsolete override

 -- Rafael Laboissière <rafael@debian.org>  Tue, 10 Nov 2020 09:21:06 -0300

octave-linear-algebra (2.2.3-2) unstable; urgency=medium

  * Team upload

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Sun, 02 Aug 2020 12:19:09 -0300

octave-linear-algebra (2.2.3-1) unstable; urgency=medium

  * Team upload

  * New upstream version 2.2.3
  * d/p/remove-condeig.patch: Drop patch (applied upstream)
  * d/p/octave-config.patch: Drop patch (removed upstream)
  * d/p/remove-autotools.patch: Drop patch (applied upstream)
  * d/p/F77_INT.patch: Drop patch (removed upstream)
  * d/p/pgmres-unit-test-deterministic.patch: Drop patch (removed upstream)
  * d/p/errwarn-instead-of-gripes.patch: Drop patch (removed upstream)
  * d/copyright: Reflect upstream changes
  * d/control: Declare Architecture: all

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 15 Nov 2019 04:23:57 -0300

octave-linear-algebra (2.2.2-9) unstable; urgency=medium

  * Team upload

  * d/control: Bump dependency on dh-octave to >= 0.7.1
    This allows the injection of the virtual package octave-abi-N into the
    package's list of dependencies

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 07 Nov 2019 04:45:47 -0300

octave-linear-algebra (2.2.2-8) unstable; urgency=medium

  * Team upload

  * d/p/errwarn-instead-of-gripes.patch: New patch.
    This patch allows the compilation against Octave 5.1.
  * d/control: Bump Standards-Version to 4.4.0 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 05 Oct 2019 08:15:48 -0300

octave-linear-algebra (2.2.2-7) unstable; urgency=medium

  * Team upload

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:56:24 -0200

octave-linear-algebra (2.2.2-6) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/p/pgmres-unit-test-deterministic.patch: Add patch (Closes: #902382)

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 29 Jun 2018 16:42:04 +0200

octave-linear-algebra (2.2.2-5) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Rafael Laboissiere ]
  * d/control: Bump Standards-Version to 4.1.4 (no changes needed)

  [ Sébastien Villemot ]
  * remove-condeig.patch: new patch taken from upstream
  * Fix FTBFS against Octave 4.4, using several upstream patches
    + F77_INT.patch
    + octave-config.patch
    + remove-autotools.patch

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 10 Jun 2018 22:00:35 +0200

octave-linear-algebra (2.2.2-4) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:32:50 -0200

octave-linear-algebra (2.2.2-3) unstable; urgency=medium

  * Team upload.

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.
  * d/control: use cgit instead of gitweb in Vcs-Browser.

  [ Rafael Laboissiere ]
  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:44 -0200

octave-linear-algebra (2.2.2-2) unstable; urgency=medium

  * Team upload

  * d/o-l-a.lintian-overrides: Drop override for no-fortify hardening
  * d/control:
    + Use secure URIs in the Vcs-* fields
    + Bump Standards-Version to 3.9.8 (no changes needed)
  * d/s/options: New file (ignore files changed by the build process)

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 15 Aug 2016 14:38:09 -0300

octave-linear-algebra (2.2.2-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.2.2
  * d/p/fix-tests.patch: Drop patch (applied upstream)
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * Bump build-dependency on octave-pkg-dev, for proper unit testing
  * Drop build-dependency on octave-general

  [ Sébastien Villemot ]
  * mkoctfile-depends.patch: remove patch.
    It is no longer needed since #770192 is now fixed.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 19 Jul 2015 21:17:36 +0200

octave-linear-algebra (2.2.0-3) unstable; urgency=medium

  * mkoctfile-depends.patch: new patch, drops the call to mkoctfile -M.
    Should fix FTBFS on armel, armhf, powerpc, ppc64el, s390x.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 24 Sep 2014 09:44:38 +0200

octave-linear-algebra (2.2.0-2) unstable; urgency=medium

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

  [ Rafael Laboissiere ]
  * Remove obsolete DM-Upload-Allowed flag
  * debian/copyright: Use octave-maintainers mailing list as upstream contact

  [ Sébastien Villemot ]
  * Use my @debian.org email address
  * Uploaders: remove Thomas Weber and Rafael Laboissiere.
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 22 Sep 2014 18:15:26 +0200

octave-linear-algebra (2.2.0-1) unstable; urgency=low

  * Imported Upstream version 2.2.0
  * autoload-yes.patch: new patch
  * fix-tests.patch: new patch
  * Add Build-Depends on octave-general >= 1.3.0
  * debian/copyright: reflect upstream changes
  * debian/watch: use SourceForge redirector
  * Add lintian overrides for false positives on hardening build flags

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Mon, 21 May 2012 21:44:31 +0200

octave-linear-algebra (2.1.0-2) unstable; urgency=low

  * debian/copyright: use final URL for machine-readable copyright format 1.0
  * Bump Standards-Version to 3.9.3, no changes needed
  * Bump to debhelper compat level 9
  * Build-depend on octave-pkg-dev >= 1.0.0 in order to build against Octave 3.6

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sat, 10 Mar 2012 22:10:17 +0100

octave-linear-algebra (2.1.0-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 2.1.0
  * debian/rules: bump to debhelper compat level 8
  * Remove obsolete README.source
  * Bump to Standards-Version 3.9.2, no changes needed
  * Add myself to Uploaders
  * debian/copyright: update according to upstream changes and latest DEP5
  * debian/check.m: remove obsolete file (gsvd tests are now in .cc source)

 -- Thomas Weber <tweber@debian.org>  Mon, 21 Nov 2011 22:47:10 +0100

octave-linear-algebra (2.0.0-2) unstable; urgency=low

  * Upload to unstable

 -- Thomas Weber <tweber@debian.org>  Thu, 14 Apr 2011 20:57:40 +0200

octave-linear-algebra (2.0.0-1) experimental; urgency=low

  * New upstream release
  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571850)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Switch to dpkg-source 3.0 (quilt) format

 -- Thomas Weber <tweber@debian.org>  Thu, 02 Sep 2010 22:40:07 +0200

octave-linear-algebra (1.0.8-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * New upstream release
  * Removed patch smwsolve-norm-in-test.diff, no more necessary for octave3.2
  * debian/rules:
    + permissions of cartprod.m were fixed upstream, remove code
    + Use debian/clean for cleaning, remove code

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 06 Dec 2009 23:11:23 +0100

octave-linear-algebra (1.0.7-1) unstable; urgency=low

  * New upstream release
  * debian/patches/smwsolve-norm-in-test.diff: Add patch for making the
    test of smwsolve succeed
  * debian/control:
    + (Build-Depends): Add quilt
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
  * debian/rules:
    + Include patchsys-quilt.mk
    + Fix wrong permission of script cartprod.m
  * debian/copyright: Use DEP5 URL in Format-Specification

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 15:01:42 +0200

octave-linear-algebra (1.0.6-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sat, 04 Apr 2009 23:10:48 +0200

octave-linear-algebra (1.0.6-1) experimental; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * Modified the watch file so uscan will find a match.
  * debian/control: Bumped Standards-Version to 3.8.0 (no changes
    needed)

  [ Thomas Weber ]
  * New upstream release
  * Bump dependency on octave-pkg-dev to 0.6.1, to get the experimental
    version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Tue, 09 Dec 2008 22:16:36 +0100

octave-linear-algebra (1.0.5-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 12 May 2008 10:23:28 +0200

octave-linear-algebra (1.0.4-1) unstable; urgency=low

  * Initial release (closes: #468508)

 -- Ólafur Jens Sigurðsson <ojsbug@gmail.com>  Sun, 10 Feb 2008 22:13:30 +0100
