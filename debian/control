Source: octave-linear-algebra
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-octave
Standards-Version: 4.7.2
Homepage: https://gnu-octave.github.io/packages/linear-algebra/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-linear-algebra.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-linear-algebra
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-linear-algebra
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}, ${octave:Depends}
Description: additional linear-algebra functions for Octave
 This package provides additional functions to work on linear algebra
 code in Octave, a numerical computation software. This package
 includies general singular value methods to factorize a matrix
 (bicg), function to calculate the condition numbers of eigenvalues of
 a matrix (condeig), a matrix equivalent of the Octave function name
 (funm) and trigonometric/hyperbolic functions for square matrixes.
 .
 This Octave add-on package is part of the Octave-Forge project.
